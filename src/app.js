const express = require('express')
const app = express()
var pjson = require('../package.json');
app.use('/', express.static('public'))
app.get('/version', (req, res) => res.send(pjson.version))


app.listen(3000, () => console.log('Example app listening on port 3000!'))